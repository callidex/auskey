﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IdentityModel.Tokens;
using System.Net;
using System.Reflection;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using Abr.AuskeyManager.KeyStore;
using System.IdentityModel.Protocols.WSTrust;
using System.Linq;
using System.ServiceModel.Configuration;
using RSAAUSKEY.UsiCreateServiceReference;

namespace RSAAUSKEY
{
    static class ServiceChannel
    {
        // Name of the /configuration/system.servicemodel/client/endpoint configuration used in
        // the App.config to configure the ChannelFactory for the USI service.
        const string EndpointConfigurationName = "WS2007FederationHttpBinding_IUSIService";

        private static ChannelFactory<IUSIService> _channelFactory;

        public static IUSIService OpenWithM2M(string alias, string keystoreFilePath, string password)
        {
            SecurityToken token = GetStsToken(tokenLifeTimeMinutes: 60, alias, keystoreFilePath, password);

            var clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
            var endpointElement = clientSection.Endpoints.Cast<ChannelEndpointElement>().First(endpoint =>
                "UsiCreateServiceReference.IUSIService".Equals(endpoint.Contract, StringComparison.InvariantCultureIgnoreCase));
            if (endpointElement == null)
                throw new Exception("No endpoint matching service contract was found");

            var channelFactory = new ChannelFactory<IUSIService>(endpointElement.Name);
            channelFactory.Open();

            return channelFactory.CreateChannelWithIssuedToken(token);
        }

        private static SecurityToken GetStsToken(int tokenLifeTimeMinutes, string alias, string keystoreFilePath, string password)
        {
            WS2007HttpBinding binding = new WS2007HttpBinding();
            binding.Security.Message.ClientCredentialType = MessageCredentialType.Certificate;
            binding.Security.Mode = SecurityMode.TransportWithMessageCredential;
            binding.Security.Message.EstablishSecurityContext = false;
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Basic256Sha256;
            string endPoint = "S007SecurityTokenServiceEndpointV3";

            var factory = new WSTrustChannelFactory(endPoint);
            factory.Credentials.ClientCertificate.Certificate = GetClientCertificateFromKeystore(alias, keystoreFilePath, password);

            // Instantiate and invoke the client to get the security token
            factory.Credentials.SupportInteractive = false;

            var appliesTo = "https://portal.usi.gov.au/Service/v3/UsiCreateService.svc";
            var rst = new RequestSecurityToken
            {
                Claims = { new RequestClaim("http://vanguard.ebusiness.gov.au/2008/06/identity/claims/abn", false),
                    new RequestClaim("http://vanguard.ebusiness.gov.au/2008/06/identity/claims/credentialtype", false) },
                AppliesTo = new EndpointReference(appliesTo),
                Lifetime = new Lifetime(DateTime.UtcNow, DateTime.UtcNow.AddMinutes(tokenLifeTimeMinutes)),
                RequestType = RequestTypes.Issue,
                KeyType = KeyTypes.Symmetric,
                TokenType = "http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV1.1"
            };

            // Instantiate and invoke the client to get the security token
            var client = (WSTrustChannel)factory.CreateChannel();

            SecurityToken response = client.Issue(rst);

            return response;
        }

        internal static X509Certificate2 GetClientCertificateFromKeystore(string alias, string keystoreFilePath, string password)
        {
            //Please replace [YourCompanyName] tag with the valid organisation Name
            AbrProperties.SetSoftwareInfo("[OrganisationName]", Assembly.GetEntryAssembly().GetName().Name,
                Assembly.GetEntryAssembly().GetName().Version.ToString(), DateTime.Now.ToString(CultureInfo.InvariantCulture));

            var keyStore = new AbrKeyStore(File.OpenRead(keystoreFilePath));

            using (var pwd = GetPasswordString(password))
            {
                var abrCredential = keyStore.GetCredential(alias);

                abrCredential.IsReadyForRenewal();
                X509Certificate2 clientCertificate = abrCredential.PrivateKey(pwd, X509KeyStorageFlags.MachineKeySet);
                return clientCertificate;
            }
        }

        private static SecureString GetPasswordString(string password)
        {
            var pwd = new SecureString();
                foreach (var c in password)
                    pwd.AppendChar(c);

                return pwd;
        }

        public static void Close()
        {
            if (_channelFactory != null)
                _channelFactory.Close();

            _channelFactory = null;
        }
    }
}


