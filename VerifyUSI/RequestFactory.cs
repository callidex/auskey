﻿using System;
using RSAAUSKEY.UsiCreateServiceReference;

namespace RSAAUSKEY
{
    static class RequestFactory
    {
        static Random _random = new Random();

        public static BulkVerifyUSIRequest CreateBulkVerifyRequest(VerificationType[] verifications, string orgCode)
        {
            var bulkVerifyUSIType = new BulkVerifyUSIType
            {
                OrgCode = orgCode,
                NoOfVerifications = verifications.Length,
                Verifications = verifications
            };

            return new BulkVerifyUSIRequest(bulkVerifyUSIType);
        }

        public static VerificationType CreateVerification(
            int recordId,
            string USI,
            string firstName,
            string lastName,
            DateTime dateOfBirth)
        {
            var verificationType = new VerificationType
            {
                RecordId = recordId,
                DateOfBirth = dateOfBirth,
                ItemsElementName = new[] { ItemsChoiceType1.FirstName, ItemsChoiceType1.FamilyName },
                Items = new[] { firstName, lastName },
                USI = USI
            };
            return verificationType;
        }
    }
}


