﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using RSAAUSKEY.UsiCreateServiceReference;

namespace RSAAUSKEY
{
    class Program
    {
        static int Main(string[] args)
        {
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(CustomSSLCertificateValidation);
            string output = null;
            const string orgCode = "40592";
            if (args.Length != 7)
            {
                Console.WriteLine($"Incorrect args ({args.Length}: Alias, KeyStore, Password, USI, Firstname, LastName, Date of Birth");
                return 1;
            }
            else
            {
                output = PerformVerify(args[0], args[1], args[2], orgCode, args[3].ToUpper(), args[4], args[5], args[6]);
                Console.WriteLine(output);
                return 0;
            }
        }

        static bool CustomSSLCertificateValidation(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            return error == SslPolicyErrors.None;
        }

        private static string DecodeBulkVerifyResponse(VerificationResponseType response)
        {
            string message = string.Format("USI={1}\nUSIStatus={2}\n", response.RecordId, response.USI, response.USIStatus);
            for (int i = 0; i < response.Items.Count(); i++)
            {
                message += string.Format("{0}={1}\n", response.ItemsElementName[i], response.Items[i]);
            }
            return message += string.Format("DateOfBirth={0}\n", response.DateOfBirth);
        }

        static string PerformVerify(string alias, string keystoreFilePath, string password, string orgCode, string usiToTest, string firstName, string lastName, string date)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12;

            IUSIService client = null;
            string result = null;
            StringBuilder sb = new StringBuilder();
            try
            {

                // Create an array of applications.
                var request = RequestFactory.CreateBulkVerifyRequest(new VerificationType[]
                {
                    RequestFactory.CreateVerification(1, usiToTest, firstName, lastName, DateTime.ParseExact(date, "yyyy-MM-dd", CultureInfo.InvariantCulture))}
                , orgCode);

                // Open a channel to USI service.
                client = ServiceChannel.OpenWithM2M(alias, keystoreFilePath, password);

                BulkVerifyUSIResponse response;
                try
                {
                    response = client.BulkVerifyUSI(request);
                }
                catch (FaultException<ErrorInfo> ex)
                {
                    return string.Format("USI={0}\nUSIStatus=Invalid\n", usiToTest);
                    //sb.AppendLine("VerifyUSI returned a FaultException");
                    //sb.AppendLine(string.Format("Detail: {0}", ex.Detail.Message));
                    //result = sb.ToString();
                    //return result;
                }
                catch(Exception e)
                {
                    return string.Format("USI={0}\nUSIStatus=Invalid\n", usiToTest);
                }

                var responseStrings = response.BulkVerifyUSIResponse1.VerificationResponses.Select(DecodeBulkVerifyResponse);
                string messages = string.Join(string.Format("{0}{0}", Environment.NewLine), responseStrings);
                result = messages;
            }
            finally
            {
                if (client != null)
                    ((ICommunicationObject)client).Close();

                ServiceChannel.Close();
            }
            return result;
        }
    }
}

